# NoButtonBlock
- Lunar Client modifies the `GuiConnecting#actionPerformed` method to not close the gui when the cancel button is pressed when it is in certain states. **This mod reverts the behaviour back to its default.**

![](asset/image/GuiConnectingScreen.png)

<br>

## Installation
1. Download the [Astral](#download) mod.
2. Download the corresponding version of [Zenith Core](https://gitlab.com/candicey-weave/zenith-core/-/releases) that is mentioned in the release notes.
3. Place the jars in your Weave mods folder.
   1. Windows: `%userprofile%\.lunarclient\mods`
   2. Unix: `~/.lunarclient/mods`
4. Download [Weave Manager](https://github.com/exejar/Weave-Manager).

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.

<br>

## License
- NoButtonBlock is licensed under the [GNU General Public License Version 3](https://gitlab.com/candicey-weave/NoButtonBlock/-/blob/master/LICENSE).