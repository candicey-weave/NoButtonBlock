import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version ("1.8.0")

    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

group = "com.gitlab.candicey.nobuttonblock"
version = "1.0.0"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))

    compileOnly("com.github.Weave-MC:Weave-Loader:70bd82faa6")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}