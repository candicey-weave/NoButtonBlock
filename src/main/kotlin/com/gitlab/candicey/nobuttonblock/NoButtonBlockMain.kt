package com.gitlab.candicey.nobuttonblock

import net.weavemc.loader.api.ModInitializer
import org.apache.logging.log4j.LogManager

class NoButtonBlockMain : ModInitializer {
    override fun preInit() {
        LogManager
            .getLogger("NoButtonBlock")
            .info("[NoButtonBlock] NoButtonBlock has been loaded!")
    }
}