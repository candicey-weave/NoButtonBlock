package com.gitlab.candicey.nobuttonblock.extension

import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.multiplayer.GuiConnecting
import net.minecraft.network.NetworkManager

private val cancelField = GuiConnecting::class.java.getDeclaredField("cancel")
var GuiConnecting.cancel: Boolean
    get() = cancelField.getBoolean(this)
    set(value) {
        cancelField.setBoolean(this, value)
    }

private val networkManagerField = GuiConnecting::class.java.getDeclaredField("networkManager")
var GuiConnecting.networkManager: NetworkManager?
    get() = networkManagerField.get(this) as? NetworkManager
    set(value) {
        networkManagerField.set(this, value)
    }

private val previousGuiScreenField = GuiConnecting::class.java.getDeclaredField("previousGuiScreen")
val GuiConnecting.previousGuiScreen: GuiScreen?
    get() = previousGuiScreenField.get(this) as? GuiScreen