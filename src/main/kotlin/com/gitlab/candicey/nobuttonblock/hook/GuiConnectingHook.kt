package com.gitlab.candicey.nobuttonblock.hook

import com.gitlab.candicey.nobuttonblock.extension.cancel
import com.gitlab.candicey.nobuttonblock.extension.networkManager
import com.gitlab.candicey.nobuttonblock.extension.previousGuiScreen
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.multiplayer.GuiConnecting
import net.minecraft.util.ChatComponentText
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LabelNode

class GuiConnectingHook : Hook("net/minecraft/client/multiplayer/GuiConnecting") {
    companion object {
        /**
         * Lunar Client modifies the [GuiConnecting.actionPerformed] method to sometimes not close the gui when the cancel button is pressed.
         */
        @JvmStatic
        fun onActionPerformed(guiConnecting: GuiConnecting, button: GuiButton): Boolean {
            if (button.id == 0) {
                guiConnecting.cancel = true
                guiConnecting.networkManager?.closeChannel(ChatComponentText("Aborted"))
                Minecraft.getMinecraft().displayGuiScreen(guiConnecting.previousGuiScreen)

                return true
            }

            return false
        }
    }

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.find { it.name == "actionPerformed" }!!.instructions.insert(asm {
            val label = LabelNode()
            aload(0)
            aload(1)
            invokestatic(
                "com/gitlab/candicey/nobuttonblock/hook/GuiConnectingHook",
                "onActionPerformed",
                "(Lnet/minecraft/client/multiplayer/GuiConnecting;Lnet/minecraft/client/gui/GuiButton;)Z"
            )
            ifeq(label)
            _return
            +label
        })

        cfg.computeFrames()
    }
}